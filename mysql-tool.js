'strict'
const mysql = require('mysql');
var connection = mysql.createConnection({
	  host     : '192.168.124.10',
	  port     : '37273',  
	  user     : 'root',
	  password : 'root1234',
	  database : 'note'
	});
	
	
const execSql = (sql, params) =>{
	return new Promise((resolve, reject) => {
		connection.query(sql, params, (err, data) => {
			if(err) {
				reject(err)
			} else {
				resolve(data)
			}
		})
	})
}
	
module.exports = execSql;

