

$(function (){
    $(window).bind("scroll",function (){
        let top = $(window).scrollTop();
        if(top > 10){
            $(".content-left").addClass("moveTop")
        }else{
			$(".content-left").removeClass("moveTop")
		}
    });
	

	$("#currCity").mouseover(function(){
		$("#contents").css({"display":"block"});
	});
	$("#currCity").mouseout(function(){
		$("#contents").css({"display":"none"});
	});
	
	
	getAction();
    loadTool('');
	
	getWeather();
})

function getAction(){
	let param = window.location.search;
	if(param != ""){
		let action = param.substring(1);
		$.ajax({
			url:"/getAction?action="+action,
			type:"get",
			success: function (res){
				if(res.data){
					$('.top-content').append(`<span style="display: inline-block;float: right;cursor: pointer;margin-right: 10px;" onclick="openAddTool();">新增</span>`);
				}
			}
		});
	}
}

function loadTool(id){
    $.ajax({
        url:"/loadTool",
        type:"get",
        data:{"id":id},
        success: function (res){
            loadToolItem(res.data);
        }
    });

}
function loadToolItem(list){
    for(let i=0;i<list.length;i++) {
        let obj = list[i];
        let html = `
            <div class="item-title">
                <span onclick="loadGrid(${obj.id})">${obj.name}</span>
            </div>
        `;
        $("#content-item").append(html);
    }
}

function openWin(url){
	window.open(url);
}

function openAdvise(){
    $(".isMask").show();
    $(".isAdvise").show();
}
function advise(){
    let advise = $("#advise").val();
    if(advise === '' || advise == null){
        return alert("请输入您的建议");
    }
    $.ajax({
        url:"/advise",
        type:"post",
        data:{"advise":advise},
        success: function (res){
            $(".isMask").hide();
            $(".isAdvise").hide();
        }
    });
}

function openAddTool(){
    $(".isMask").show();
    $(".isAdd").show();
    loadSelect();
}

function loadSelect(){
    $.ajax({
        url:"/loadTool",
        type:"get",
        success: function (res){
			let list = res.data;
			$("#tool_type").empty();
            for(let i=0;i<list.length;i++){
                let obj = list[i];
                let html = `<option value="${obj.id}">${obj.name}</option>`;
                $("#tool_type").append(html);
            }
        }
    });
}
function addTool(){
    let tool_type = $("#tool_type").val();
    if(tool_type == ""){
        return alert("工具类型 不能为空");
    }
    let tool_imgSrc = $("#tool_imgSrc").val();
    if(tool_imgSrc == ""){
        return alert("网站图标路径 不能为空");
    }
    let tool_h3 = $("#tool_h3").val();
    if(tool_h3 == ""){
        return alert("工具名称 不能为空");
    }
    let tool_target = $("#tool_target").val();
    if(tool_target == ""){
        return alert("网站地址 不能为空");
    }
    
    let obj = {
        "toolID":tool_type,
        "imgSrc":tool_imgSrc,
        "h3":tool_h3,
        "target":tool_target
    };
    $.ajax({
        url:"/addTool",
        type:"post",
        data:obj,
        success:function (res){
            $(".isMask").hide();
            $(".isAdd").hide();
			$("#tool_imgSrc").val('');
			$("#tool_h3").val('');
			$("#tool_target").val('');
        }
    });
}
function closeFun(){
    $(".isAdvise").hide();
    $(".isMask").hide();
    $(".isAdd").hide();
}
function getWeather(){
		let time = new Date().getTime();
		$.ajax({
			url:"https://www.baidu.com/home/other/data/weatherInfo?city=&indextype=manht&_req_seqid=0xbfaaeb7e0006a31b&asyn=1&sid=26350&t="+time,
			type:"get",
			dataType:"jsonp",
			success:function(res){
				$("#contents").empty();
				let data = res.data.weather.content;
				let city = data.ipcity;
				let condition = data.today.condition;
				$("#currCity").append(city + ' ' + condition);
				let jieri = data.calendar.festival == false ? "":("("+data.calendar.festival+")");
				
				let html = `
					<div style="color: #333;display: flex; justify-content: space-between;">
						<span>${data.ipcity}  ${data.week}  农历 ${data.calendar.lunar} ${jieri}</span>
					</div>
					<div style="margin-top:10px;">
						<div style="width:84px;float:left;text-align:center;">
							<div>${data.today.date}</div>
							<div><img width="32px" src="https://dss3.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/weather/icons2/${data.today.imgs[0]}.png"/></div>
							<div>${data.today.temp}</div>
							<div>${data.today.condition}</div>
							<div>${data.today.wind}</div>
						</div>
						<div style="width:84px;float:left;text-align:center;">
							<div>${data.tomorrow.date}</div>
							<div><img width="32px" src="https://dss3.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/weather/icons2/${data.tomorrow.imgs[0]}.png"/></div>
							<div>${data.tomorrow.temp}</div>
							<div>${data.tomorrow.condition}</div>
							<div>${data.tomorrow.wind}</div>
						</div>
						<div style="width:84px;float:left;text-align:center;">
							<div>${data.thirdday.date}</div>
							<div><img width="32px" src="https://dss3.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/weather/icons2/${data.thirdday.imgs[0]}.png"/></div>
							<div>${data.thirdday.temp}</div>
							<div>${data.thirdday.condition}</div>
							<div>${data.thirdday.wind}</div>
						</div>
						<div style="width:84px;float:left;text-align:center;">
							<div>${data.fourthday.date}</div>
							<div><img width="32px" src="https://dss3.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/weather/icons2/${data.fourthday.imgs[0]}.png"/></div>
							<div>${data.fourthday.temp}</div>
							<div>${data.fourthday.condition}</div>
							<div>${data.fourthday.wind}</div>
						</div>
						<div style="width:84px;float:left;text-align:center;">
							<div>${data.fifthday.date}</div>
							<div><img width="32px" src="https://dss3.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/weather/icons2/${data.fifthday.imgs[0]}.png"/></div>
							<div>${data.fifthday.temp}</div>
							<div>${data.fifthday.condition}</div>
							<div>${data.fifthday.wind}</div>
						</div>
					</div>
				`;
				$("#contents").append(html);
			}
		})
	}
	
