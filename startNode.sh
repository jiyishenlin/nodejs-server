#!/bin/bash
pid=`ps -ef|grep node | grep -v 'grep' | awk '{print $2}'`
if [ -n "$pid" ];then
   kill -9 $pid
fi

nohup node index.js >./info.log 2>&1 &
echo 'ok'
